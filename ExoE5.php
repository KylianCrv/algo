<html>
    <head>
        <title>Index</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../CSS/sIndex.css">
    </head>
    <body>

        <?php
        $a = 0;

        if (isset($_POST["btn_valider"])) {
            $a = $_POST["entier"];
            $a = $a * $a * $a;

            printf("la valeur du cube du nombre que vous avez choisi est égale à %d", $a);
        }
        ?>
        <form action="ExoE5.php" method="POST">
            Saisissez un nombre entier : <input type="text" name="entier" pattern="[0-9]{1,}"> <br>
            <input type="submit" name="btn_valider" value="Valider">
        </form>
    </body>

    <!--
    A est un entier ;
    Ecrire "veuillez saisir un entier A :"

    Lire (A);
    A<- a*a*a;

    Afficher "la valeur du cube de A est égale à %d, $a"