<html>
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../CSS/sIndex.css">
    </head>
    <body>
        <?php
        $a = 0;
        $b = 0;

        if (isset($_POST["btn_envoi"])) {

            $a = $_POST["entier1"];
            $b = $_POST["entier2"];
            $a = $a + 1;
            $b = $b + 5;
            $a = $a + 1;
            $b = $b + 5;
            $a = $a + 1;
            $b = $b + 5;
            printf("valeur de a = %d et valeur de b = %d", $a, $b);
        }
        ?>

        <form action="Exo4E.php" method="POST">
            Entier 1 : <input type="text" pattern="[0-9]{1,}" name="entier1"> <br>
            Entier 2 : <input type="text" pattern="[0-9]{1,}" name="entier2"> <br>
            <input type="submit" name="btn_envoi" value="Valider">
        </form>
    </body>


    <!-- Pseudo langage :
    A et B sont entiers

    Ecrire "veuillez saisir deux nombres entiers :" ;
    Lire (A) ; En prenant A = 37
    Lire (B) ; B = 64
    A <- A + 1 ; A = 37 + 1 (38)
    B <- B + 5; B = 64 + 5 (69)
    A <- A+1; A = 38 + 1 (39)
    B <- B+5; B = 69 + 5 (74)
    A <- A+1; A = 39 + 1 (40)
    B <- B+5; B = 74 + 5 (79)

    Afficher ("A = %d B = %d", $a, $b) -->